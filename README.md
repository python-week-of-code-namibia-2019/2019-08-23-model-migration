# Python Week Of Code, Namibia 2019

[blog/models.py](blog/models.py) has the source code of the database model.

## Task for Instructor

1. Add

   ```
   tags = models.CharField()  # as comma separate values
   ```

   to the class `Post`.
2. Create a new migration file by running

   ```
   python manage.py makemigrations
   ```

   And migrate the database by running

   ```
   python manage.py migrate
   ```
2. Create three blog post

   ```
   from blog.models import Post
   post = Post(title="Welcome", text="This is our first blog post.", tags="Alice,Bob,Eve")
   post.save()
   post = Post(title="Safari", text="Explore landscape and wildlife by car.", tags="Alice,safari")
   post.save()
   post = Post(title="Camping", text="In the national parks there are places without any amenities where you even have to bring water.", tags="Bob,camping")
   post.save()
   ```
3. Add

   ```
   contributed = models.BooleanField(default=False)
   ```

   to the class `Post`.
4. Create a new migration file by running

   ```
   python manage.py makemigrations
   ```

5. Add function to run during migration.

   ```
   def fill_contributed(apps, schema_editor):
       Post = apps.get_model('blog', 'Post')
       for post in Post.objects.all():
           tags = post.tags.split(",")
           for tag in tags:
               if tag not in ["Alice", "Bob"]:
                   post.contributed = True
                   post.save()


   class Migration(migrations.Migration):

       dependencies = [
           ('blog', '0002_post_tags'),
       ]

       operations = [
           migrations.AddField(
               model_name='post',
               name='contributed',
               field=models.BooleanField(default=False),
           ),
           migrations.RunPython(fill_contributed),
       ]
   ```

   to `blog/migrations/0003_post_contributed.py`
6. Migrate the database by running

   ```
   python manage.py migrate
   ```

## Tasks for Learners

1. Create a field named `authors` and move some tags from `tags` to `authors`.