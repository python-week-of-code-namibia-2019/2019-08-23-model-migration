from django.db import models
from django.utils import timezone

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=280)  # To be compatible with Twitter
    text = models.TextField()
    created_date = models.DateTimeField(
        default=timezone.now
    )
    published_date = models.DateTimeField(
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        self.published_date = timezone.now()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
